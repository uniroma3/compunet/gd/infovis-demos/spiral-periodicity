
function Spiral(graphType) {
  this.option = {
    graphType: graphType || "points",
    numberOfPoints: null,
    period: null,
    margin: {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0
    },
    svgHeight: 0,
    svgWidth: 0,
    spacing: 1,
    lineWidth: 50,
    targetElement: '#chart',
    data: [],
    x: d3.scaleLinear().range([0, 1000]),
    y: d3.scaleLinear().range([1000, 0]),
    color: 'black',
    colorMode: 'opacity'
  }
}; 


Spiral.prototype.cartesian = function(radius, angle, size) {
  var classObj = this;
  var option = classObj.option;

  var size = size || 1;
  var xPos = option.x(radius * Math.cos(angle));
  var yPos = option.y(radius * Math.sin(angle));
  return [xPos, yPos, size];
};


//Funzione render 
Spiral.prototype.render = function() {
  var classObj = this;
  var option = classObj.option;
  var end;
  var svg = d3.select(option.targetElement)
  //var color = d3.scaleSequential(d3.interpolatePuBuGn);
  
  .append("svg")
  .attr("width", option.svgWidth)
  .attr("height", option.svgHeight)

 //Inizializzazione della spirale
  option.data.forEach(function(datum, t){

    var start = startAngle(t, option.period);
    end = endAngle(t, option.period);

    var startCenter = radius(option.spacing, start ) ;
    var endCenter = radius(option.spacing, end) ;
    var startInnerRadius =  startCenter - option.lineWidth*0.5;
    var startOuterRadius =  startCenter + option.lineWidth*0.5;
    var endInnerRadius =  endCenter - option.lineWidth*0.5;
    var endOuterRadius = endCenter + option.lineWidth*0.5;

    var ctrlInnerRad = 0.01;
    var ctrlOuterRad = 0.01;
    var angle =  theta(t, option.period);
    var rad = radius(option.spacing, angle);
    var innerControlPoint = classObj.cartesian(rad - option.lineWidth*0.5 + ctrlInnerRad, angle);
    var outerControlPoint = classObj.cartesian(rad + option.lineWidth*0.5 + ctrlOuterRad, angle);

    var startPoint =   classObj.cartesian(startInnerRadius, start);
    var point2 =  classObj.cartesian(startOuterRadius, start);
    var point3 =   classObj.cartesian(endOuterRadius, end);
    var point4 = classObj.cartesian(endInnerRadius, end);
    var arcPath = "M"+startPoint[0]+" "+startPoint[1]+"L"+point2[0]+" "+point2[1];
    arcPath += "Q"+outerControlPoint[0]+" "+outerControlPoint[1]+" "+point3[0]+" "+point3[1];
    arcPath += "L"+point4[0]+" "+point4[1];
    arcPath += "Q"+innerControlPoint[0]+" "+innerControlPoint[1]+" "+startPoint[0]+" "+startPoint[1]+"Z";
    datum[0] = arcPath
  });

  svg.append("g")
  .attr("transform", "translate(" + option.margin.left + "," + option.margin.top + ")");

//Visualizza la spirale creata 
  var max = d3.max(option.data, function(d) { return +d[2] });
  
  svg.selectAll("g").selectAll("path")
  .data(option.data)
  .enter().append("path")
  .style("fill", function(d) { return colorSelector(d,max); })
  .style("opacity", "0")
  .attr("d", function(d) { return d[0]}) //disegna spirale 
  .on("mouseover", function(d) { details(d[1],d[2]) });

//Funzione per l'animazione
   svg.selectAll("path")
      .transition()
      .delay(function(d,i){
        return i*5;
      })
      .duration(2)
      .style("opacity","1");

}; 

//Funzione di setting dei parametri della spirale
Spiral.prototype.setParam = function(param, value) {
  var classObj = this;
  var option = classObj.option;

  option[param] = value;

  if (['svgHeight', 'svgWidth', 'margin.top', 'margin.right', 'margin.bottom', 'margin.left'].indexOf(param) > -1) {
    var width = option.svgWidth - option.margin.left - option.margin.right;
    var height = option.svgHeight - option.margin.top - option.margin.bottom;
    option.x = d3.scaleLinear().range([0, width]).domain([-option.svgWidth, option.svgWidth]);
    option.y = d3.scaleLinear().range([height, 0]).domain([-option.svgHeight, option.svgHeight]);
  }
};

//Funzione redrawing della spirale al variare del value, sovrapponendo quella precedente
Spiral.prototype.redraw = function() {
  var classObj = this;
  var option = classObj.option;

  var spiralContainer = document.getElementById(option.targetElement.substr(1));
  while (spiralContainer.firstChild) {
    spiralContainer.removeChild(spiralContainer.firstChild)
  }
  classObj.render();
};

//Metodi di supporto per il disegno della spirale

function theta(t, period) {
  return   Math.PI/2 + (2*Math.PI / period *  t);
};

function startAngle(t, period) {
  return ( theta(t-1, period) +  theta(t, period))/2;
};

function endAngle(t, period) {
  return (theta(t+1, period) + theta(t, period))/2;
};

function radius(spacing, angle) {
  return 300 + (spacing * angle);
}

//Gestisce il colore di ogni rettangolo
//In: value corrente e massimo value. Out: fattore colore definito tra 0 e 1
function colorSelector(datum,max) {
    
  var temp = datum[2];

    return color(temp/max);
}

//Visualizza i dettagli correnti con la funzione mouseover
function details(day,temp){
    
  d3.select(".day span").text(day); 
  d3.select(".temp span").text(temp); 
  
}

