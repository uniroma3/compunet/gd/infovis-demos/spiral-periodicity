# Spiral periodicity

A spiral visualization that allows you to find periodicity in time-series data


## Todos

- Update the project with a more recent version of D3.js
- Allow the user to choose different kinds of default datasets
- Allow the user to load/save a dataset to the client harddisk
- Describe the file format

## Online demo

Can be found here: https://uniroma3.gitlab.io/compunet/gd/infovis-demos/spiral-periodicity

## Credits

Demo realized by Marco Venturini for the final project of Information Visualization at Roma Tre University.
